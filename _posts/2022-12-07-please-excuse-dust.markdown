---
layout: post
title:  "Please excuse my dust!"
date:   2022-12-07 14:40:57 +0700
categories: migration
---

<img height="400px" src="/assets/img/rubocop-stairs.jpeg" alt="Stairs at a Beach">

Hi ruby style lovers. I am Peter Boling, author and maintainer of the `rubocop-lts` family of ruby gems.
The whole family is in the midst of a major move from [GitHub][github] to [GitLab][gitlab].
Please excuse my temporary dust and broken links.
I am also working on a gem that will have a suite of tools to assist
projects with the laborious and error-prone migration process.  I'll link to it once published,
but I am already using it with the migrations done thus far.  Yummy dog food.

[github]: https://github.com/rubocop-lts
[gitlab]: https://gitlab.com/rubocop-lts
