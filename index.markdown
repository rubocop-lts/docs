---
# Feel free to add content and custom Front Matter to this file.
# To modify the layout, see https://jekyllrb.com/docs/themes/#overriding-theme-defaults

layout: home
title: Home
---

<p align="center">
    <a href="https://rubocop.org#gh-light-mode-only"  target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/rubocop-light.svg?raw=true" alt="SVG RuboCop Logo, Copyright (c) 2014 Dimiter Petrov, CC BY-NC 4.0, see docs/images/logo/README.txt">
    </a>
    <a href="https://www.ruby-lang.org/" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/ruby-logo.svg?raw=true" alt="Yukihiro Matsumoto, Ruby Visual Identity Team, CC BY-SA 2.5, see docs/images/logo/README.txt">
    </a>
    <a href="https://semver.org/#gh-light-mode-only" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/semver-light.svg?raw=true" alt="SemVer.org Logo by @maxhaz, see docs/images/logo/README.txt">
    </a>
</p>

# 🦾 RuboCop LTS - Rules for Minimum Ruby Versions: Rubocop + Standard + Betterlint + Shopify + Gradual & More

💡 See the intro [blog post](https://dev.to/pboling/rubocop-ruby-matrix-gems-nj)!

---

The **RuboCop LTS** family of gems is the distillation of almost 20 years
of my own Ruby expertise and source code diving,
built on the shoulders of the expertise of many others;
organizing that expertise into per-Ruby-version sets of configurations.

Although the situation has improved somewhat,
it remains [_unsafe_ to upgrade RuboCop, or Standard][Why-Build-This],
in a project that supports EOL Rubies.

I hope it helps others avoid some of the challenges I've had with library maintenance,
and supporting decade-old mission-critical applications.

Avoid bike-shedding, use `rubocop-lts` in every project, and
let it manage your linting complexity!

If the `rubocop-lts` stack of libraries has helped you, or your organization,
please support my efforts by making a donation, becoming a sponsor, or giving me a shout on Mastodon.

[Why-Build-This]: https://rubocop-lts.gitlab.io/about/#why-build-this-

[![Liberapay Patrons][⛳liberapay-img]][⛳liberapay]
[![Sponsor Me on Github][🖇sponsor-img]][🖇sponsor]

<span class="badge-buymeacoffee">
<a href="https://ko-fi.com/O5O86SNP4" target='_blank' title="Donate to my FLOSS or refugee efforts at ko-fi.com"><img src="https://img.shields.io/badge/buy%20me%20coffee-donate-yellow.svg" alt="Buy me coffee donation button" /></a>
</span>
<span class="badge-patreon">
<a href="https://patreon.com/galtzo" title="Donate to my FLOSS or refugee efforts using Patreon"><img src="https://img.shields.io/badge/patreon-donate-yellow.svg" alt="Patreon donate button" /></a>
</span>

<a rel="me" alt="Follow me on Ruby.social" href="https://ruby.social/@galtzo"><img src="https://img.shields.io/mastodon/follow/109447111526622197?domain=https%3A%2F%2Fruby.social&style=social&label=Follow%20%40galtzo%20on%20Ruby.social"></a>
<a rel="me" alt="Follow me on FLOSS.social" href="https://floss.social/@galtzo"><img src="https://img.shields.io/mastodon/follow/110304921404405715?domain=https%3A%2F%2Ffloss.social&style=social&label=Follow%20%40galtzo%20on%20Floss.social"></a>

[⛳liberapay-img]: https://img.shields.io/liberapay/patrons/pboling.svg?logo=liberapay
[⛳liberapay]: https://liberapay.com/pboling/donate
[🖇sponsor-img]: https://img.shields.io/badge/Sponsor_Me!-pboling.svg?style=social&logo=github
[🖇sponsor]: https://github.com/sponsors/pboling

---

## Table of Contents

* 👩‍💻 [Org Health](#org-health-)
* 👪 [A Gem Family](#a-gem-family-)
* ✨ [Installation](#installation-)
  * [Install for RubyGem](#install-for-rubygem)
  * [Install for Rails Application](#install-for-rails-application)
* 🔧 [Usage](#usage-)
  * [Ruby with RSpec](#ruby-with-rspec)
  * [Ruby without RSpec](#ruby-without-rspec)
  * [Rails with RSpec](#rails-with-rspec)
  * [Rails without RSpec](#rails-without-rspec)
  * [RubyGem with RSpec](#rubygem-with-rspec)
  * [RubyGem without RSpec](#rubygem-without-rspec)
  * [Raketasks](#rake-tasks)
  * [Dependabot](#dependabot)
  * [Version - Branch Matrix](#version---branch-matrix)
* ⚡️ [Contributing](#contributing-)
* 🌈 [Contributors](#contributors-)
* 📄 [License](#license-)
  * © [Copyright](#copyright-)
* 🤝 [Code of Conduct](#code-of-conduct-)
* 📌 [Versioning](#versioning-)

**Background & Theory**

* 🌱 [Why Build This][Why-Build-This]
* 🌱 [Convention > Configuration][Convention-Over-Configuration]
  * 🌱 [Even Releases][Even-Major-Release]
* 🌱 [How to Upgrade Ruby (1.8 to 3.2)!][How-To-Upgrade-Ruby]

[Convention-Over-Configuration]: https://rubocop-lts.gitlab.io/CONV_OVER_CONF/
[Even-Major-Release]: https://rubocop-lts.gitlab.io/CONV_OVER_CONF/#even-major-release
[How-To-Upgrade-Ruby]: https://rubocop-lts.gitlab.io/HOW_TO_UPGRADE_RUBY/

## A Gem Family 👪

The `rubocop-lts` family of gems has a version supporting any version of Ruby you need.
They should be used as development dependencies for rubygems, libraries, or applications.

The project is distributed across multiple source hosts
(i.e. [actual DVCS](https://railsbling.com/posts/dvcs/put_the_d_in_dvcs/)),
because source hosts have a history of not lasting long term,
or [becoming hostile](https://railsbling.com/posts/dvcs/give_up_github/) to the ideals of open source.

Star the ones you use!
Fork if you have an idea that would help the community of maintainers of old Ruby code.

| Project                                | GitLab                                                                                                                                      | GitHub                                                                                                                                      |
|----------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|
| [RuboCop LTS Org][⛳️ltso-gl]           |                                                                                                                                             | [![GitHub Org's stars][⛳️ltso-ghs-i]][⛳️ltso-gh]                                                                                            |
| [`rubocop-lts`][⛳️lts-gl]              | [![rubocop-lts GitLab stars][⛳️lts-gls-i]][⛳️lts-gl] [![rubocop-lts GitLab forks][⛳️lts-glf-i]][⛳️lts-gl]                                   | [![rubocop-lts GitHub stars][⛳️lts-ghs-i]][⛳️lts-gh] [![rubocop-lts GitHub forks][⛳️lts-ghf-i]][⛳️lts-gh]                                   |
| [`standard-rubocop-lts`][⛳️stdrlts-gl] | [![standard-rubocop-lts GitLab stars][⛳️stdrlts-gls-i]][⛳️stdrlts-gl] [![standard-rubocop-lts GitLab forks][⛳️stdrlts-glf-i]][⛳️stdrlts-gl] | [![standard-rubocop-lts GitHub stars][⛳️stdrlts-ghs-i]][⛳️stdrlts-gh] [![standard-rubocop-lts GitHub forks][⛳️stdrlts-ghf-i]][⛳️stdrlts-gh] |
| [`rubocop-ruby1_8`][⛳️18-gl]           | [![rubocop-ruby1_8 GitLab stars][⛳️18-gls-i]][⛳️18-gl] [![rubocop-ruby1_8 GitLab forks][⛳️18-glf-i]][⛳️18-gl]                               | [![rubocop-ruby1_8 GitHub stars][⛳️18-ghs-i]][⛳️18-gh] [![rubocop-ruby1_8 GitHub forks][⛳️18-ghf-i]][⛳️18-gh]                               |
| [`rubocop-ruby1_9`][⛳️19-gl]           | [![rubocop-ruby1_9 GitLab stars][⛳️19-gls-i]][⛳️19-gl] [![rubocop-ruby1_9 GitLab forks][⛳️19-glf-i]][⛳️19-gl]                               | [![rubocop-ruby1_9 GitLab stars][⛳️19-ghs-i]][⛳️19-gh] [![rubocop-ruby1_9 GitLab forks][⛳️19-ghf-i]][⛳️19-gh]                               |
| [`rubocop-ruby2_0`][⛳️20-gl]           | [![rubocop-ruby2_0 GitLab stars][⛳️20-gls-i]][⛳️20-gl] [![rubocop-ruby2_0 GitLab forks][⛳️20-glf-i]][⛳️20-gl]                               | [![rubocop-ruby2_0 GitLab stars][⛳️20-ghs-i]][⛳️20-gh] [![rubocop-ruby2_0 GitLab forks][⛳️20-ghf-i]][⛳️20-gh]                               |
| [`rubocop-ruby2_1`][⛳️21-gl]           | [![rubocop-ruby2_1 GitLab stars][⛳️21-gls-i]][⛳️21-gl] [![rubocop-ruby2_1 GitLab forks][⛳️21-glf-i]][⛳️21-gl]                               | [![rubocop-ruby2_1 GitLab stars][⛳️21-ghs-i]][⛳️21-gh] [![rubocop-ruby2_1 GitLab forks][⛳️21-ghf-i]][⛳️21-gh]                               |
| [`rubocop-ruby2_2`][⛳️22-gl]           | [![rubocop-ruby2_2 GitLab stars][⛳️22-gls-i]][⛳️22-gl] [![rubocop-ruby2_2 GitLab forks][⛳️22-glf-i]][⛳️22-gl]                               | [![rubocop-ruby2_2 GitLab stars][⛳️22-ghs-i]][⛳️22-gh] [![rubocop-ruby2_2 GitLab forks][⛳️22-ghf-i]][⛳️22-gh]                               |
| [`rubocop-ruby2_3`][⛳️23-gl]           | [![rubocop-ruby2_3 GitLab stars][⛳️23-gls-i]][⛳️23-gl] [![rubocop-ruby2_3 GitLab forks][⛳️23-glf-i]][⛳️23-gl]                               | [![rubocop-ruby2_3 GitLab stars][⛳️23-ghs-i]][⛳️23-gh] [![rubocop-ruby2_3 GitLab forks][⛳️23-ghf-i]][⛳️23-gh]                               |
| [`rubocop-ruby2_4`][⛳️24-gl]           | [![rubocop-ruby2_4 GitLab stars][⛳️24-gls-i]][⛳️24-gl] [![rubocop-ruby2_4 GitLab forks][⛳️24-glf-i]][⛳️24-gl]                               | [![rubocop-ruby2_4 GitLab stars][⛳️24-ghs-i]][⛳️24-gh] [![rubocop-ruby2_4 GitLab forks][⛳️24-ghf-i]][⛳️24-gh]                               |
| [`rubocop-ruby2_5`][⛳️25-gl]           | [![rubocop-ruby2_5 GitLab stars][⛳️25-gls-i]][⛳️25-gl] [![rubocop-ruby2_5 GitLab forks][⛳️25-glf-i]][⛳️25-gl]                               | [![rubocop-ruby2_5 GitLab stars][⛳️25-ghs-i]][⛳️25-gh] [![rubocop-ruby2_5 GitLab forks][⛳️25-ghf-i]][⛳️25-gh]                               |
| [`rubocop-ruby2_6`][⛳️26-gl]           | [![rubocop-ruby2_6 GitLab stars][⛳️26-gls-i]][⛳️26-gl] [![rubocop-ruby2_6 GitLab forks][⛳️26-glf-i]][⛳️26-gl]                               | [![rubocop-ruby2_6 GitLab stars][⛳️26-ghs-i]][⛳️26-gh] [![rubocop-ruby2_6 GitLab forks][⛳️26-ghf-i]][⛳️26-gh]                               |
| [`rubocop-ruby2_7`][⛳️27-gl]           | [![rubocop-ruby2_7 GitLab stars][⛳️27-gls-i]][⛳️27-gl] [![rubocop-ruby2_7 GitLab forks][⛳️27-glf-i]][⛳️27-gl]                               | [![rubocop-ruby2_7 GitLab stars][⛳️27-ghs-i]][⛳️27-gh] [![rubocop-ruby2_7 GitLab forks][⛳️27-ghf-i]][⛳️27-gh]                               |
| [`rubocop-ruby3_0`][⛳️30-gl]           | [![rubocop-ruby3_0 GitLab stars][⛳️30-gls-i]][⛳️30-gl] [![rubocop-ruby3_0 GitLab forks][⛳️30-glf-i]][⛳️30-gl]                               | [![rubocop-ruby3_0 GitLab stars][⛳️30-ghs-i]][⛳️30-gh] [![rubocop-ruby3_0 GitLab forks][⛳️30-ghf-i]][⛳️30-gh]                               |
| [`rubocop-ruby3_1`][⛳️31-gl]           | [![rubocop-ruby3_1 GitLab stars][⛳️31-gls-i]][⛳️31-gl] [![rubocop-ruby3_1 GitLab forks][⛳️31-glf-i]][⛳️31-gl]                               | [![rubocop-ruby3_1 GitLab stars][⛳️31-ghs-i]][⛳️31-gh] [![rubocop-ruby3_1 GitLab forks][⛳️31-ghf-i]][⛳️31-gh]                               |
| [`rubocop-ruby3_2`][⛳️32-gl]           | [![rubocop-ruby3_2 GitLab stars][⛳️32-gls-i]][⛳️32-gl] [![rubocop-ruby3_2 GitLab forks][⛳️32-glf-i]][⛳️32-gl]                               | [![rubocop-ruby3_2 GitLab stars][⛳️32-ghs-i]][⛳️32-gh] [![rubocop-ruby3_2 GitLab forks][⛳️32-ghf-i]][⛳️32-gh]                               |

[⛳️ltso-gl]: https://gitlab.com/rubocop-lts
[⛳️ltso-glf-i]: https://img.shields.io/gitlab/stars/rubocop-lts
[⛳️ltso-gh]: https://github.com/rubocop-lts
[⛳️ltso-ghs-i]: https://img.shields.io/github/stars/rubocop-lts
[⛳️ltso-ghf-i]: https://img.shields.io/github/forks/rubocop-lts

[⛳️lts-gl]: https://gitlab.com/rubocop-lts/rubocop-lts
[⛳️lts-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-lts
[⛳️lts-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-lts
[⛳️lts-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-lts
[⛳️lts-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-lts

[⛳️stdrlts-gl]: https://gitlab.com/rubocop-lts/standard-rubocop-lts
[⛳️stdrlts-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Fstandard-rubocop-lts
[⛳️stdrlts-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Fstandard-rubocop-lts
[⛳️stdrlts-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/standard-rubocop-lts
[⛳️stdrlts-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/standard-rubocop-lts

[⛳️18-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby1_8
[⛳️18-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby1_8
[⛳️18-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby1_8
[⛳️18-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby1_8
[⛳️18-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby1_8

[⛳️19-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby1_9
[⛳️19-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby1_9
[⛳️19-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby1_9
[⛳️19-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby1_9
[⛳️19-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby1_9

[⛳️20-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_0
[⛳️20-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_0
[⛳️20-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_0
[⛳️20-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_0
[⛳️20-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_0

[⛳️21-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_1
[⛳️21-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_1
[⛳️21-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_1
[⛳️21-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_1
[⛳️21-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_1

[⛳️22-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_2
[⛳️22-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_2
[⛳️22-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_2
[⛳️22-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_2
[⛳️22-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_2

[⛳️23-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_3
[⛳️23-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_3
[⛳️23-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_3
[⛳️23-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_3
[⛳️23-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_3

[⛳️24-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_4
[⛳️24-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_4
[⛳️24-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_4
[⛳️24-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_4
[⛳️24-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_4

[⛳️25-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_5
[⛳️25-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_5
[⛳️25-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_5
[⛳️25-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_5
[⛳️25-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_5

[⛳️26-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_6
[⛳️26-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_6
[⛳️26-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_6
[⛳️26-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_6
[⛳️26-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_6

[⛳️27-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby2_7
[⛳️27-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby2_7
[⛳️27-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby2_7
[⛳️27-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby2_7
[⛳️27-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby2_7

[⛳️30-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby3_0
[⛳️30-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby3_0
[⛳️30-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby3_0
[⛳️30-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby3_0
[⛳️30-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby3_0

[⛳️31-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby3_1
[⛳️31-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby3_1
[⛳️31-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby3_1
[⛳️31-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby3_1
[⛳️31-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby3_1

[⛳️32-gl]: https://gitlab.com/rubocop-lts/rubocop-ruby3_2
[⛳️32-gls-i]: https://img.shields.io/gitlab/stars/rubocop-lts%2Frubocop-ruby3_2
[⛳️32-glf-i]: https://img.shields.io/gitlab/forks/rubocop-lts%2Frubocop-ruby3_2
[⛳️32-ghs-i]: https://img.shields.io/github/stars/rubocop-lts/rubocop-ruby3_2
[⛳️32-ghf-i]: https://img.shields.io/github/forks/rubocop-lts/rubocop-ruby3_2

## Org Health 👩‍💻

| Gem Name                     | Version                             | Downloads                                                            | CI                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | Activity                                                                                                                                              |
|------------------------------|-------------------------------------|----------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------|
| [`rubocop-lts`][⛳️lts-gh]    | [![Gem Version][⛳️lts-vi]][⛳️lts-g] | [![Total DL][🖇️lts-dti]][⛳️lts-g] [![DL Rank][🏘️lts-rti]][⛳️lts-g] | Main: [![Current][🚎lts-cwfi]][🚎lts-cwf]<br>v0, ruby1.8+: [![1.8 even][🚎lts-1_8e-cwfi]][🚎lts-1_8e-cwf]<br>v2, ruby1.9+: [![1.9 even][🚎lts-1_9e-cwfi]][🚎lts-1_9e-cwf]<br>v4, ruby2.0+: [![2.0 even][🚎lts-2_0e-cwfi]][🚎lts-2_0e-cwf]<br>v6, ruby2.1+: [![2.1 even][🚎lts-2_1e-cwfi]][🚎lts-2_1e-cwf]<br>v8, ruby2.2+: [![2.2 even][🚎lts-2_2e-cwfi]][🚎lts-2_2e-cwf]<br>v10, ruby2.3+: [![2.3 even][🚎lts-2_3e-cwfi]][🚎lts-2_3e-cwf]<br>v12, ruby2.4+: [![2.4 even][🚎lts-2_4e-cwfi]][🚎lts-2_4e-cwf]<br>v14, ruby2.5+: [![2.5 even][🚎lts-2_5e-cwfi]][🚎lts-2_5e-cwf]<br>v16, ruby2.6+: [![2.6 even][🚎lts-2_6e-cwfi]][🚎lts-2_6e-cwf]<br>v18, ruby2.7+: [![2.7 even][🚎lts-2_7e-cwfi]][🚎lts-2_7e-cwf]<br>v20, ruby3.0+: [![3.0 even][🚎lts-3_0e-cwfi]][🚎lts-3_0e-cwf]<br>v22, ruby3.1+: [![3.1 even][🚎lts-3_1e-cwfi]][🚎lts-3_1e-cwf]<br>v24, ruby3.2+: [![3.2 even][🚎lts-3_2e-cwfi]][🚎lts-3_2e-cwf] | [![Open Issues][📗lts-ioi]][📗lts-io] [![Closed Issues][🚀lts-ici]][🚀lts-ic] [![Open PRs][💄lts-poi]][💄lts-po] [![Closed PRs][👽lts-pci]][👽lts-pc] |
| [`rubocop-ruby1_8`][⛳️18-gh] | [![Gem Version][⛳️18-vi]][⛳️18-g]   | [![Total DL][🖇️18-dti]][⛳️18-g] [![DL Rank][🏘️18-rti]][⛳️19-g]     | [![Current][🚎18-cwfi]][🚎18-cwf] [![Heads][🖐18-hwfi]][🖐18-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗19-ioi]][📗19-io] [![Closed Issues][🚀19-ici]][🚀19-ic] [![Open PRs][💄19-poi]][💄19-po] [![Closed PRs][👽19-pci]][👽19-pc]         |
| [`rubocop-ruby1_9`][⛳️19-gh] | [![Gem Version][⛳️19-vi]][⛳️19-g]   | [![Total DL][🖇️19-dti]][⛳️19-g] [![DL Rank][🏘️19-rti]][⛳️19-g]     | [![Current][🚎19-cwfi]][🚎19-cwf] [![Heads][🖐19-hwfi]][🖐19-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗19-ioi]][📗19-io] [![Closed Issues][🚀19-ici]][🚀19-ic] [![Open PRs][💄19-poi]][💄19-po] [![Closed PRs][👽19-pci]][👽19-pc]         |
| [`rubocop-ruby2_0`][⛳️20-gh] | [![Gem Version][⛳️20-vi]][⛳️20-g]   | [![Total DL][🖇️20-dti]][⛳️20-g] [![DL Rank][🏘️20-rti]][⛳️20-g]     | [![Current][🚎20-cwfi]][🚎20-cwf] [![Heads][🖐20-hwfi]][🖐20-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗20-ioi]][📗20-io] [![Closed Issues][🚀20-ici]][🚀20-ic] [![Open PRs][💄20-poi]][💄20-po] [![Closed PRs][👽20-pci]][👽20-pc]         |
| [`rubocop-ruby2_1`][⛳️21-gh] | [![Gem Version][⛳️21-vi]][⛳️21-g]   | [![Total DL][🖇️21-dti]][⛳️21-g] [![DL Rank][🏘️21-rti]][⛳️21-g]     | [![Current][🚎21-cwfi]][🚎21-cwf] [![Heads][🖐21-hwfi]][🖐21-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗21-ioi]][📗21-io] [![Closed Issues][🚀21-ici]][🚀21-ic] [![Open PRs][💄21-poi]][💄21-po] [![Closed PRs][👽21-pci]][👽21-pc]         |
| [`rubocop-ruby2_2`][⛳️22-gh] | [![Gem Version][⛳️22-vi]][⛳️22-g]   | [![Total DL][🖇️22-dti]][⛳️22-g] [![DL Rank][🏘️22-rti]][⛳️22-g]     | [![Current][🚎22-cwfi]][🚎22-cwf] [![Heads][🖐22-hwfi]][🖐22-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗22-ioi]][📗22-io] [![Closed Issues][🚀22-ici]][🚀22-ic] [![Open PRs][💄22-poi]][💄22-po] [![Closed PRs][👽22-pci]][👽22-pc]         |
| [`rubocop-ruby2_3`][⛳️23-gh] | [![Gem Version][⛳️23-vi]][⛳️23-g]   | [![Total DL][🖇️23-dti]][⛳️23-g] [![DL Rank][🏘️23-rti]][⛳️23-g]     | [![Current][🚎23-cwfi]][🚎23-cwf] [![Heads][🖐23-hwfi]][🖐23-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗23-ioi]][📗23-io] [![Closed Issues][🚀23-ici]][🚀23-ic] [![Open PRs][💄23-poi]][💄23-po] [![Closed PRs][👽23-pci]][👽23-pc]         |
| [`rubocop-ruby2_4`][⛳️24-gh] | [![Gem Version][⛳️24-vi]][⛳️24-g]   | [![Total DL][🖇️24-dti]][⛳️24-g] [![DL Rank][🏘️24-rti]][⛳️24-g]     | [![Current][🚎24-cwfi]][🚎24-cwf] [![Heads][🖐24-hwfi]][🖐24-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗24-ioi]][📗24-io] [![Closed Issues][🚀24-ici]][🚀24-ic] [![Open PRs][💄24-poi]][💄24-po] [![Closed PRs][👽24-pci]][👽24-pc]         |
| [`rubocop-ruby2_5`][⛳️25-gh] | [![Gem Version][⛳️25-vi]][⛳️25-g]   | [![Total DL][🖇️25-dti]][⛳️25-g] [![DL Rank][🏘️25-rti]][⛳️25-g]     | [![Current][🚎25-cwfi]][🚎25-cwf] [![Heads][🖐25-hwfi]][🖐25-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗25-ioi]][📗25-io] [![Closed Issues][🚀25-ici]][🚀25-ic] [![Open PRs][💄25-poi]][💄25-po] [![Closed PRs][👽25-pci]][👽25-pc]         |
| [`rubocop-ruby2_6`][⛳️26-gh] | [![Gem Version][⛳️26-vi]][⛳️26-g]   | [![Total DL][🖇️26-dti]][⛳️26-g] [![DL Rank][🏘️26-rti]][⛳️26-g]     | [![Current][🚎26-cwfi]][🚎26-cwf] [![Heads][🖐26-hwfi]][🖐26-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗26-ioi]][📗26-io] [![Closed Issues][🚀26-ici]][🚀26-ic] [![Open PRs][💄26-poi]][💄26-po] [![Closed PRs][👽26-pci]][👽26-pc]         |
| [`rubocop-ruby2_7`][⛳️27-gh] | [![Gem Version][⛳️27-vi]][⛳️27-g]   | [![Total DL][🖇️27-dti]][⛳️27-g] [![DL Rank][🏘️27-rti]][⛳️27-g]     | [![Current][🚎27-cwfi]][🚎27-cwf] [![Heads][🖐27-hwfi]][🖐27-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗27-ioi]][📗27-io] [![Closed Issues][🚀27-ici]][🚀27-ic] [![Open PRs][💄27-poi]][💄27-po] [![Closed PRs][👽27-pci]][👽27-pc]         |
| [`rubocop-ruby3_0`][⛳️30-gh] | [![Gem Version][⛳️30-vi]][⛳️30-g]   | [![Total DL][🖇️30-dti]][⛳️30-g] [![DL Rank][🏘️30-rti]][⛳️30-g]     | [![Current][🚎30-cwfi]][🚎30-cwf] [![Heads][🖐30-hwfi]][🖐30-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗30-ioi]][📗30-io] [![Closed Issues][🚀30-ici]][🚀30-ic] [![Open PRs][💄30-poi]][💄30-po] [![Closed PRs][👽30-pci]][👽30-pc]         |
| [`rubocop-ruby3_1`][⛳️31-gh] | [![Gem Version][⛳️31-vi]][⛳️31-g]   | [![Total DL][🖇️31-dti]][⛳️31-g] [![DL Rank][🏘️31-rti]][⛳️31-g]     | [![Current][🚎31-cwfi]][🚎31-cwf] [![Heads][🖐31-hwfi]][🖐31-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗31-ioi]][📗31-io] [![Closed Issues][🚀31-ici]][🚀31-ic] [![Open PRs][💄31-poi]][💄31-po] [![Closed PRs][👽31-pci]][👽31-pc]         |
| [`rubocop-ruby3_2`][⛳️32-gh] | [![Gem Version][⛳️32-vi]][⛳️32-g]   | [![Total DL][🖇️32-dti]][⛳️32-g] [![DL Rank][🏘️32-rti]][⛳️32-g]     | [![Current][🚎32-cwfi]][🚎32-cwf] [![Heads][🖐32-hwfi]][🖐32-hwf]                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 | [![Open Issues][📗32-ioi]][📗32-io] [![Closed Issues][🚀32-ici]][🚀32-ic] [![Open PRs][💄32-poi]][💄32-po] [![Closed PRs][👽32-pci]][👽32-pc]         |
| [`standard-rubocop-lts`][⛳️stdrlts-gh] | [![Gem Version][⛳️stdrlts-vi]][⛳️stdrlts-g] | [![Total DL][🖇️stdrlts-dti]][⛳️stdrlts-g] [![DL Rank][🏘️stdrlts-rti]][⛳️stdrlts-g] | [![Current][🚎stdrlts-cwfi]][🚎stdrlts-cwf] [![Heads][🖐stdrlts-hwfi]][🖐stdrlts-hwf] | [![Open Issues][📗stdrlts-ioi]][📗stdrlts-io] [![Closed Issues][🚀stdrlts-ici]][🚀stdrlts-ic] [![Open PRs][💄stdrlts-poi]][💄stdrlts-po] [![Closed PRs][👽stdrlts-pci]][👽stdrlts-pc] |

## Installation ✨

Without bundler execute:

    $ gem install rubocop-lts

Add this line to your application's Gemfile:

```ruby
gem 'rubocop-lts', '~> X.0', require: false
```
NOTE: If you are on Rails, do not add `require: false`, 
or the Railtie which loads the `rubocop_gradual` rake task will not load.

NOTE: Figure out what `X` you need from the [Version - Branch Matrix](#version---branch-matrix) table.

### Install for RubyGem

In your `*.gemspec`
```ruby
# See table below for the right X based on the library's minimum Ruby version
spec.add_development_dependency 'rubocop-lts', '~> X.0'

# Add ⬇️ **additional** ⬇️ gem dependencies:
# IMPORTANT: rubocop-packaging's rules allow your gem to be packaged for linux distributions!
spec.add_development_dependency 'rubocop-packaging', '~> 0.5', '>= 0.5.2'

# Using RSpec? Add ☑️ **required** ☑️ gem dependencies (only required if using one of the RSpec configs)
spec.add_development_dependency 'rubocop-rspec', '~> 2.25'
spec.add_development_dependency 'rspec-block_is_expected', '~> 1.0', '>= 1.0.5'
```

### Install for Rails Application

Rails-specific linting gems are not dependencies of this gem because Rails is just one way to build among many.
If you are using Rails, please add them!

```ruby
# Add ⬇️ **additional** ⬇️ gem dependencies for Rails-specific linting
group :development, :test do
  gem "standard-rails"
  gem "betterlint"
end
```

And then execute:

    $ bundle

## Usage 🔧

In your `.rubocop.yml` do the following:

🔥 Delete the following line 🔥
```diff
- inherit_from: .rubocop_todo.yml
```

You can probably 🔥 delete 🔥 most of the other lines too.
This tool is anti-bike-shedding.
You no longer need to worry about the rules!

🔥 Any of the following configs are no longer needed 🔥
```diff
- require:
-   - 'betterlint'
-   - 'rubocop-md'
-   - 'rubocop-packaging'
-   - 'rubocop-performance'
-   - 'rubocop-rake'
-   - 'rubocop-rspec'
-   - 'rubocop-rails'
-   - 'rubocop-thread_safety'
-   - 'rubocop/gradual/patch'
-   - 'standard'
-   - 'standard-custom'
-   - 'standard-performance'
-   - 'standard-rails'
-   - 'standard-rubocop-lts'
- 
- AllCops:
-   NewCops: enable
-   DisplayCopNames: true
-   TargetRubyVersion: X.X
```

Then pick one of the following to add!

### Ruby with RSpec

```yaml
inherit_gem:
  rubocop-lts: rubocop-lts.yml # for the ruby + rspec
  # NOTE: this is the default, and as such, is equivalent to:
  # rubocop-lts: config/ruby_rspec.yml
```

### Ruby (without RSpec)

```yaml
inherit_gem:
  rubocop-lts: config/ruby.yml
```

### Rails with RSpec

```yaml
inherit_gem:
  rubocop-lts: config/rails_rspec.yml
```

### Rails (without RSpec)

```yaml
inherit_gem:
  rubocop-lts: config/rails.yml
```

### RubyGem with RSpec

```yaml
inherit_gem:
  rubocop-lts: config/rubygem_rspec.yml
```

### RubyGem (without RSpec)

```yaml
inherit_gem:
  rubocop-lts: config/rubygem.yml
```

### Rake Tasks

NOTE: On Rails it is **automatic**, via Railtie, so you can skip this.

In a non-Rails environment add the following to your `Rakefile`:
```ruby
require "rubocop/lts"
Rubocop::Lts.install_tasks
```

This will load the `rubocop_gradual` rake task, and alias it as `rubocop`.

### Dependabot Noise Reduction

Pull Request noise reduction is possible!

Add the following to `.github/dependabot.yml` if you use Github Actions.

```yaml
    ignore:
      - dependency-name: "rubocop-lts"
```

## Git Branch Names

These won't matter unless you want to contribute to the project, or you are adding the dependency via git.

### Naming Scheme

```ruby
"r{ ruby major }_{ ruby minor }-{ parity }-v{ gem-version }"
```

#### Parity

Even versions use the token `even` in the branch name.  Please upgrade to an even release if not already on one!
Odd versions use the token `odd` in the branch name, and are _all deprecated_.  No further odd releases are planned.

### Version - Branch Matrix

| Ruby Version | Major Version Num Parity | Gem Version | Branch Name     |
|--------------|--------------------------|-------------|-----------------|
| 1.8          | even                     | 0.x         | `r1_8-even-v0`  |
| 1.9          | even                     | 2.x         | `r1_9-even-v2`  |
| 2.0          | even                     | 4.x         | `r2_0-even-v4`  |
| 2.1          | even                     | 6.x         | `r2_1-even-v6`  |
| 2.2          | even                     | 8.x         | `r2_2-even-v8`  |
| 2.3          | even                     | 10.x        | `r2_3-even-v10` |
| 2.4          | even                     | 12.x        | `r2_4-even-v12` |
| 2.5          | even                     | 14.x        | `r2_5-even-v14` |
| 2.6          | even                     | 16.x        | `r2_6-even-v16` |
| 2.7          | even                     | 18.x        | `r2_7-even-v18` |
| 3.0          | even                     | 20.x        | `r3_0-even-v20` |
| 3.1          | even                     | 22.x        | `r3_1-even-v22` |
| 3.2          | even                     | 24.x        | `r3_2-even-v24` |

## Contributing ⚡️

See [CONTRIBUTING.md][contributing]

## Contributors 🌈

[![Contributors](https://contrib.rocks/image?repo=rubocop-lts/rubocop-lts)]("https://github.com/rubocop-lts/rubocop-lts/graphs/contributors")

Made with [contributors-img](https://contrib.rocks).

## License 📄

The gem is available as open source under the terms of
the [MIT License][license] [![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)][license-ref].
See [LICENSE][license] for the official [Copyright Notice][copyright-notice-explainer].

<details>
  <summary>Project Logos (rubocop-lts)</summary>

See [docs/images/logo/README.txt][project-logos]
</details>

<details>
  <summary>Organization Logo (rubocop-lts)</summary>

Author: [Yusuf Evli][org-logo-author]
Source: [Unsplash][org-logo-source]
License: [Unsplash License][org-logo-license]
</details>

[project-logos]: https://github.com/rubocop-lts/rubocop-lts/blob/main/docs/images/logo/README.txt
[org-logo-author]: https://unsplash.com/@yusufevli
[org-logo-source]: https://unsplash.com/photos/yaSLNLtKRIU
[org-logo-license]: https://unsplash.com/license

### Copyright ©

* Copyright (c) 2022 - 2023 [Peter H. Boling][peterboling] of [Rails Bling][railsbling]

## Code of Conduct 🤝

Everyone interacting in the RuboCop LTS codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://github.com/rubocop-lts/rubocop-lts/blob/main/CODE_OF_CONDUCT.md).

## Versioning 📌

This library aims to adhere to [Semantic Versioning 2.0.0][semver]. Violations of this scheme should be reported as
bugs. Specifically, if a minor or patch version is released that breaks backward compatibility, a new version should be
immediately released that restores compatibility. Breaking changes to the public API will only be introduced with new
major versions.

As a result of this policy, you can (and should) specify a dependency on this gem using
the [Pessimistic Version Constraint][pvc] with two digits of precision.

For example:

<!-- FIND VERSION -->
```ruby
spec.add_dependency "rubocop-lts", "~> X.0"
```


<!-- columnar badge #s for Project Health table:
⛳️
🖇
🏘
🚎
🖐
🧮
📗
🚀
💄
👽
-->

[⛳️lts-vi]: http://img.shields.io/gem/v/rubocop-lts.svg
[🖇️lts-dti]: https://img.shields.io/gem/dt/rubocop-lts.svg
[🏘️lts-rti]: https://img.shields.io/gem/rt/rubocop-lts.svg
[🚎lts-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml
[🚎lts-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg
[🚎lts-1_8e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar1_8-even-v0
[🚎lts-1_8e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r1_8-even-v0
[🚎lts-1_9e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar1_9-even-v2
[🚎lts-1_9e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r1_9-even-v2
[🚎lts-2_0e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_0-even-v4
[🚎lts-2_0e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_0-even-v4
[🚎lts-2_1e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_1-even-v6
[🚎lts-2_1e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_1-even-v6
[🚎lts-2_2e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_2-even-v8
[🚎lts-2_2e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_2-even-v8
[🚎lts-2_3e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_3-even-v10
[🚎lts-2_3e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_3-even-v10
[🚎lts-2_4e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_4-even-v12
[🚎lts-2_4e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_4-even-v12
[🚎lts-2_5e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_5-even-v14
[🚎lts-2_5e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_5-even-v14
[🚎lts-2_6e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_6-even-v16
[🚎lts-2_6e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_6-even-v16
[🚎lts-2_7e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar2_7-even-v18
[🚎lts-2_7e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r2_7-even-v18
[🚎lts-3_0e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar3_0-even-v20
[🚎lts-3_0e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r3_0-even-v20
[🚎lts-3_1e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar3_1-even-v22
[🚎lts-3_1e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r3_1-even-v22
[🚎lts-3_2e-cwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml?query=branch%3Ar3_2-even-v24
[🚎lts-3_2e-cwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/current.yml/badge.svg?branch=r3_2-even-v24
[🖐lts-hwf]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/heads.yml
[🖐lts-hwfi]: https://github.com/rubocop-lts/rubocop-lts/actions/workflows/heads.yml/badge.svg
[📗lts-io]: https://github.com/rubocop-lts/rubocop-lts/issues
[📗lts-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-lts
[🚀lts-ic]: https://github.com/rubocop-lts/rubocop-lts/issues?q=is%3Aissue+is%3Aclosed
[🚀lts-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-lts
[💄lts-po]: https://github.com/rubocop-lts/rubocop-lts/pulls
[💄lts-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-lts
[👽lts-pc]: https://github.com/rubocop-lts/rubocop-lts/pulls?q=is%3Apr+is%3Aclosed
[👽lts-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-lts
[⛳️lts-g]: https://rubygems.org/gems/rubocop-lts
[⛳️lts-gh]: https://github.com/rubocop-lts/rubocop-lts
[⛳️18-vi]: http://img.shields.io/gem/v/rubocop-ruby1_8.svg
[🖇️18-dti]: https://img.shields.io/gem/dt/rubocop-ruby1_8.svg
[🏘️18-rti]: https://img.shields.io/gem/rt/rubocop-ruby1_8.svg
[🚎18-cwf]: https://github.com/rubocop-lts/rubocop-ruby1_8/actions/workflows/current.yml
[🚎18-cwfi]: https://github.com/rubocop-lts/rubocop-ruby1_8/actions/workflows/current.yml/badge.svg
[🖐18-hwf]: https://github.com/rubocop-lts/rubocop-ruby1_8/actions/workflows/heads.yml
[🖐18-hwfi]: https://github.com/rubocop-lts/rubocop-ruby1_8/actions/workflows/heads.yml/badge.svg
[📗18-io]: https://github.com/rubocop-lts/rubocop-ruby1_8/issues
[📗18-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby1_8
[🚀18-ic]: https://github.com/rubocop-lts/rubocop-ruby1_8/issues?q=is%3Aissue+is%3Aclosed
[🚀18-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby1_8
[💄18-po]: https://github.com/rubocop-lts/rubocop-ruby1_8/pulls
[💄18-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby1_8
[👽18-pc]: https://github.com/rubocop-lts/rubocop-ruby1_8/pulls?q=is%3Apr+is%3Aclosed
[👽18-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby1_8
[⛳️18-g]: https://rubygems.org/gems/rubocop-ruby1_8
[⛳️18-gh]: https://github.com/rubocop-lts/rubocop-ruby1_8
[⛳️19-vi]: http://img.shields.io/gem/v/rubocop-ruby1_9.svg
[🖇️19-dti]: https://img.shields.io/gem/dt/rubocop-ruby1_9.svg
[🏘️19-rti]: https://img.shields.io/gem/rt/rubocop-ruby1_9.svg
[🚎19-cwf]: https://github.com/rubocop-lts/rubocop-ruby1_9/actions/workflows/current.yml
[🚎19-cwfi]: https://github.com/rubocop-lts/rubocop-ruby1_9/actions/workflows/current.yml/badge.svg
[🖐19-hwf]: https://github.com/rubocop-lts/rubocop-ruby1_9/actions/workflows/heads.yml
[🖐19-hwfi]: https://github.com/rubocop-lts/rubocop-ruby1_9/actions/workflows/heads.yml/badge.svg
[📗19-io]: https://github.com/rubocop-lts/rubocop-ruby1_9/issues
[📗19-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby1_9
[🚀19-ic]: https://github.com/rubocop-lts/rubocop-ruby1_9/issues?q=is%3Aissue+is%3Aclosed
[🚀19-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby1_9
[💄19-po]: https://github.com/rubocop-lts/rubocop-ruby1_9/pulls
[💄19-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby1_9
[👽19-pc]: https://github.com/rubocop-lts/rubocop-ruby1_9/pulls?q=is%3Apr+is%3Aclosed
[👽19-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby1_9
[⛳️19-g]: https://rubygems.org/gems/rubocop-ruby1_9
[⛳️19-gh]: https://github.com/rubocop-lts/rubocop-ruby1_9
[⛳️20-vi]: http://img.shields.io/gem/v/rubocop-ruby2_0.svg
[🖇️20-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_0.svg
[🏘️20-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_0.svg
[🚎20-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_0/actions/workflows/current.yml
[🚎20-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_0/actions/workflows/current.yml/badge.svg
[🖐20-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_0/actions/workflows/heads.yml
[🖐20-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_0/actions/workflows/heads.yml/badge.svg
[📗20-io]: https://github.com/rubocop-lts/rubocop-ruby2_0/issues
[📗20-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_0
[🚀20-ic]: https://github.com/rubocop-lts/rubocop-ruby2_0/issues?q=is%3Aissue+is%3Aclosed
[🚀20-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_0
[💄20-po]: https://github.com/rubocop-lts/rubocop-ruby2_0/pulls
[💄20-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_0
[👽20-pc]: https://github.com/rubocop-lts/rubocop-ruby2_0/pulls?q=is%3Apr+is%3Aclosed
[👽20-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_0
[⛳️20-g]: https://rubygems.org/gems/rubocop-ruby2_0
[⛳️20-gh]: https://github.com/rubocop-lts/rubocop-ruby2_0
[⛳️21-vi]: http://img.shields.io/gem/v/rubocop-ruby2_1.svg
[🖇️21-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_1.svg
[🏘️21-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_1.svg
[🚎21-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_1/actions/workflows/current.yml
[🚎21-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_1/actions/workflows/current.yml/badge.svg
[🖐21-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_1/actions/workflows/heads.yml
[🖐21-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_1/actions/workflows/heads.yml/badge.svg
[📗21-io]: https://github.com/rubocop-lts/rubocop-ruby2_1/issues
[📗21-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_1
[🚀21-ic]: https://github.com/rubocop-lts/rubocop-ruby2_1/issues?q=is%3Aissue+is%3Aclosed
[🚀21-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_1
[💄21-po]: https://github.com/rubocop-lts/rubocop-ruby2_1/pulls
[💄21-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_1
[👽21-pc]: https://github.com/rubocop-lts/rubocop-ruby2_1/pulls?q=is%3Apr+is%3Aclosed
[👽21-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_1
[⛳️21-g]: https://rubygems.org/gems/rubocop-ruby2_1
[⛳️21-gh]: https://github.com/rubocop-lts/rubocop-ruby2_1
[⛳️22-vi]: http://img.shields.io/gem/v/rubocop-ruby2_2.svg
[🖇️22-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_2.svg
[🏘️22-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_2.svg
[🚎22-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_2/actions/workflows/current.yml
[🚎22-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_2/actions/workflows/current.yml/badge.svg
[🖐22-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_2/actions/workflows/heads.yml
[🖐22-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_2/actions/workflows/heads.yml/badge.svg
[📗22-io]: https://github.com/rubocop-lts/rubocop-ruby2_2/issues
[📗22-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_2
[🚀22-ic]: https://github.com/rubocop-lts/rubocop-ruby2_2/issues?q=is%3Aissue+is%3Aclosed
[🚀22-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_2
[💄22-po]: https://github.com/rubocop-lts/rubocop-ruby2_2/pulls
[💄22-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_2
[👽22-pc]: https://github.com/rubocop-lts/rubocop-ruby2_2/pulls?q=is%3Apr+is%3Aclosed
[👽22-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_2
[⛳️22-g]: https://rubygems.org/gems/rubocop-ruby2_2
[⛳️22-gh]: https://github.com/rubocop-lts/rubocop-ruby2_2
[⛳️23-vi]: http://img.shields.io/gem/v/rubocop-ruby2_3.svg
[🖇️23-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_3.svg
[🏘️23-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_3.svg
[🚎23-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_3/actions/workflows/current.yml
[🚎23-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_3/actions/workflows/current.yml/badge.svg
[🖐23-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_3/actions/workflows/heads.yml
[🖐23-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_3/actions/workflows/heads.yml/badge.svg
[📗23-io]: https://github.com/rubocop-lts/rubocop-ruby2_3/issues
[📗23-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_3
[🚀23-ic]: https://github.com/rubocop-lts/rubocop-ruby2_3/issues?q=is%3Aissue+is%3Aclosed
[🚀23-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_3
[💄23-po]: https://github.com/rubocop-lts/rubocop-ruby2_3/pulls
[💄23-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_3
[👽23-pc]: https://github.com/rubocop-lts/rubocop-ruby2_3/pulls?q=is%3Apr+is%3Aclosed
[👽23-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_3
[⛳️23-g]: https://rubygems.org/gems/rubocop-ruby2_3
[⛳️23-gh]: https://github.com/rubocop-lts/rubocop-ruby2_3
[⛳️24-vi]: http://img.shields.io/gem/v/rubocop-ruby2_4.svg
[🖇️24-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_4.svg
[🏘️24-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_4.svg
[🚎24-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_4/actions/workflows/current.yml
[🚎24-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_4/actions/workflows/current.yml/badge.svg
[🖐24-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_4/actions/workflows/heads.yml
[🖐24-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_4/actions/workflows/heads.yml/badge.svg
[📗24-io]: https://github.com/rubocop-lts/rubocop-ruby2_4/issues
[📗24-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_4
[🚀24-ic]: https://github.com/rubocop-lts/rubocop-ruby2_4/issues?q=is%3Aissue+is%3Aclosed
[🚀24-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_4
[💄24-po]: https://github.com/rubocop-lts/rubocop-ruby2_4/pulls
[💄24-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_4
[👽24-pc]: https://github.com/rubocop-lts/rubocop-ruby2_4/pulls?q=is%3Apr+is%3Aclosed
[👽24-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_4
[⛳️24-g]: https://rubygems.org/gems/rubocop-ruby2_4
[⛳️24-gh]: https://github.com/rubocop-lts/rubocop-ruby2_4
[⛳️25-vi]: http://img.shields.io/gem/v/rubocop-ruby2_5.svg
[🖇️25-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_5.svg
[🏘️25-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_5.svg
[🚎25-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_5/actions/workflows/current.yml
[🚎25-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_5/actions/workflows/current.yml/badge.svg
[🖐25-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_5/actions/workflows/heads.yml
[🖐25-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_5/actions/workflows/heads.yml/badge.svg
[📗25-io]: https://github.com/rubocop-lts/rubocop-ruby2_5/issues
[📗25-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_5
[🚀25-ic]: https://github.com/rubocop-lts/rubocop-ruby2_5/issues?q=is%3Aissue+is%3Aclosed
[🚀25-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_5
[💄25-po]: https://github.com/rubocop-lts/rubocop-ruby2_5/pulls
[💄25-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_5
[👽25-pc]: https://github.com/rubocop-lts/rubocop-ruby2_5/pulls?q=is%3Apr+is%3Aclosed
[👽25-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_5
[⛳️25-g]: https://rubygems.org/gems/rubocop-ruby2_5
[⛳️25-gh]: https://github.com/rubocop-lts/rubocop-ruby2_5
[⛳️26-vi]: http://img.shields.io/gem/v/rubocop-ruby2_6.svg
[🖇️26-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_6.svg
[🏘️26-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_6.svg
[🚎26-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_6/actions/workflows/current.yml
[🚎26-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_6/actions/workflows/current.yml/badge.svg
[🖐26-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_6/actions/workflows/heads.yml
[🖐26-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_6/actions/workflows/heads.yml/badge.svg
[📗26-io]: https://github.com/rubocop-lts/rubocop-ruby2_6/issues
[📗26-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_6
[🚀26-ic]: https://github.com/rubocop-lts/rubocop-ruby2_6/issues?q=is%3Aissue+is%3Aclosed
[🚀26-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_6
[💄26-po]: https://github.com/rubocop-lts/rubocop-ruby2_6/pulls
[💄26-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_6
[👽26-pc]: https://github.com/rubocop-lts/rubocop-ruby2_6/pulls?q=is%3Apr+is%3Aclosed
[👽26-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_6
[⛳️26-g]: https://rubygems.org/gems/rubocop-ruby2_6
[⛳️26-gh]: https://github.com/rubocop-lts/rubocop-ruby2_6
[⛳️27-vi]: http://img.shields.io/gem/v/rubocop-ruby2_7.svg
[🖇️27-dti]: https://img.shields.io/gem/dt/rubocop-ruby2_7.svg
[🏘️27-rti]: https://img.shields.io/gem/rt/rubocop-ruby2_7.svg
[🚎27-cwf]: https://github.com/rubocop-lts/rubocop-ruby2_7/actions/workflows/current.yml
[🚎27-cwfi]: https://github.com/rubocop-lts/rubocop-ruby2_7/actions/workflows/current.yml/badge.svg
[🖐27-hwf]: https://github.com/rubocop-lts/rubocop-ruby2_7/actions/workflows/heads.yml
[🖐27-hwfi]: https://github.com/rubocop-lts/rubocop-ruby2_7/actions/workflows/heads.yml/badge.svg
[📗27-io]: https://github.com/rubocop-lts/rubocop-ruby2_7/issues
[📗27-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby2_7
[🚀27-ic]: https://github.com/rubocop-lts/rubocop-ruby2_7/issues?q=is%3Aissue+is%3Aclosed
[🚀27-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby2_7
[💄27-po]: https://github.com/rubocop-lts/rubocop-ruby2_7/pulls
[💄27-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby2_7
[👽27-pc]: https://github.com/rubocop-lts/rubocop-ruby2_7/pulls?q=is%3Apr+is%3Aclosed
[👽27-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby2_7
[⛳️27-g]: https://rubygems.org/gems/rubocop-ruby2_7
[⛳️27-gh]: https://github.com/rubocop-lts/rubocop-ruby2_7
[⛳️30-vi]: http://img.shields.io/gem/v/rubocop-ruby3_0.svg
[🖇️30-dti]: https://img.shields.io/gem/dt/rubocop-ruby3_0.svg
[🏘️30-rti]: https://img.shields.io/gem/rt/rubocop-ruby3_0.svg
[🚎30-cwf]: https://github.com/rubocop-lts/rubocop-ruby3_0/actions/workflows/current.yml
[🚎30-cwfi]: https://github.com/rubocop-lts/rubocop-ruby3_0/actions/workflows/current.yml/badge.svg
[🖐30-hwf]: https://github.com/rubocop-lts/rubocop-ruby3_0/actions/workflows/heads.yml
[🖐30-hwfi]: https://github.com/rubocop-lts/rubocop-ruby3_0/actions/workflows/heads.yml/badge.svg
[📗30-io]: https://github.com/rubocop-lts/rubocop-ruby3_0/issues
[📗30-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby3_0
[🚀30-ic]: https://github.com/rubocop-lts/rubocop-ruby3_0/issues?q=is%3Aissue+is%3Aclosed
[🚀30-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby3_0
[💄30-po]: https://github.com/rubocop-lts/rubocop-ruby3_0/pulls
[💄30-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby3_0
[👽30-pc]: https://github.com/rubocop-lts/rubocop-ruby3_0/pulls?q=is%3Apr+is%3Aclosed
[👽30-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby3_0
[⛳️30-g]: https://rubygems.org/gems/rubocop-ruby3_0
[⛳️30-gh]: https://github.com/rubocop-lts/rubocop-ruby3_0
[⛳️31-vi]: http://img.shields.io/gem/v/rubocop-ruby3_1.svg
[🖇️31-dti]: https://img.shields.io/gem/dt/rubocop-ruby3_1.svg
[🏘️31-rti]: https://img.shields.io/gem/rt/rubocop-ruby3_1.svg
[🚎31-cwf]: https://github.com/rubocop-lts/rubocop-ruby3_1/actions/workflows/current.yml
[🚎31-cwfi]: https://github.com/rubocop-lts/rubocop-ruby3_1/actions/workflows/current.yml/badge.svg
[🖐31-hwf]: https://github.com/rubocop-lts/rubocop-ruby3_1/actions/workflows/heads.yml
[🖐31-hwfi]: https://github.com/rubocop-lts/rubocop-ruby3_1/actions/workflows/heads.yml/badge.svg
[📗31-io]: https://github.com/rubocop-lts/rubocop-ruby3_1/issues
[📗31-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby3_1
[🚀31-ic]: https://github.com/rubocop-lts/rubocop-ruby3_1/issues?q=is%3Aissue+is%3Aclosed
[🚀31-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby3_1
[💄31-po]: https://github.com/rubocop-lts/rubocop-ruby3_1/pulls
[💄31-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby3_1
[👽31-pc]: https://github.com/rubocop-lts/rubocop-ruby3_1/pulls?q=is%3Apr+is%3Aclosed
[👽31-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby3_1
[⛳️31-g]: https://rubygems.org/gems/rubocop-ruby3_1
[⛳️31-gh]: https://github.com/rubocop-lts/rubocop-ruby3_1
[⛳️32-vi]: http://img.shields.io/gem/v/rubocop-ruby3_2.svg
[🖇️32-dti]: https://img.shields.io/gem/dt/rubocop-ruby3_2.svg
[🏘️32-rti]: https://img.shields.io/gem/rt/rubocop-ruby3_2.svg
[🚎32-cwf]: https://github.com/rubocop-lts/rubocop-ruby3_2/actions/workflows/current.yml
[🚎32-cwfi]: https://github.com/rubocop-lts/rubocop-ruby3_2/actions/workflows/current.yml/badge.svg
[🖐32-hwf]: https://github.com/rubocop-lts/rubocop-ruby3_2/actions/workflows/heads.yml
[🖐32-hwfi]: https://github.com/rubocop-lts/rubocop-ruby3_2/actions/workflows/heads.yml/badge.svg
[📗32-io]: https://github.com/rubocop-lts/rubocop-ruby3_2/issues
[📗32-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/rubocop-ruby3_2
[🚀32-ic]: https://github.com/rubocop-lts/rubocop-ruby3_2/issues?q=is%3Aissue+is%3Aclosed
[🚀32-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/rubocop-ruby3_2
[💄32-po]: https://github.com/rubocop-lts/rubocop-ruby3_2/pulls
[💄32-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/rubocop-ruby3_2
[👽32-pc]: https://github.com/rubocop-lts/rubocop-ruby3_2/pulls?q=is%3Apr+is%3Aclosed
[👽32-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/rubocop-ruby3_2
[⛳️32-g]: https://rubygems.org/gems/rubocop-ruby3_2
[⛳️32-gh]: https://github.com/rubocop-lts/rubocop-ruby3_2
[⛳️stdrlts-vi]: http://img.shields.io/gem/v/standard-rubocop-lts.svg
[🖇️stdrlts-dti]: https://img.shields.io/gem/dt/standard-rubocop-lts.svg
[🏘️stdrlts-rti]: https://img.shields.io/gem/rt/standard-rubocop-lts.svg
[🚎stdrlts-cwf]: https://github.com/rubocop-lts/standard-rubocop-lts/actions/workflows/current.yml
[🚎stdrlts-cwfi]: https://github.com/rubocop-lts/standard-rubocop-lts/actions/workflows/current.yml/badge.svg
[🖐stdrlts-hwf]: https://github.com/rubocop-lts/standard-rubocop-lts/actions/workflows/heads.yml
[🖐stdrlts-hwfi]: https://github.com/rubocop-lts/standard-rubocop-lts/actions/workflows/heads.yml/badge.svg
[🧮stdrlts-lwf]: https://github.com/rubocop-lts/standard-rubocop-lts/actions/workflows/legacy.yml
[🧮stdrlts-lwfi]: https://github.com/rubocop-lts/standard-rubocop-lts/actions/workflows/legacy.yml/badge.svg
[📗stdrlts-io]: https://github.com/rubocop-lts/standard-rubocop-lts/issues
[📗stdrlts-ioi]: https://img.shields.io/github/issues-raw/rubocop-lts/standard-rubocop-lts
[🚀stdrlts-ic]: https://github.com/rubocop-lts/standard-rubocop-lts/issues?q=is%3Aissue+is%3Aclosed
[🚀stdrlts-ici]: https://img.shields.io/github/issues-closed-raw/rubocop-lts/standard-rubocop-lts
[💄stdrlts-po]: https://github.com/rubocop-lts/standard-rubocop-lts/pulls
[💄stdrlts-poi]: https://img.shields.io/github/issues-pr/rubocop-lts/standard-rubocop-lts
[👽stdrlts-pc]: https://github.com/rubocop-lts/standard-rubocop-lts/pulls?q=is%3Apr+is%3Aclosed
[👽stdrlts-pci]: https://img.shields.io/github/issues-pr-closed/rubocop-lts/standard-rubocop-lts
[⛳️stdrlts-g]: https://rubygems.org/gems/standard-rubocop-lts
[⛳️stdrlts-gh]: https://github.com/rubocop-lts/standard-rubocop-lts

[aboutme]: https://about.me/peter.boling
[actions]: https://github.com/rubocop-lts/rubocop-lts/actions
[angelme]: https://angel.co/peter-boling
[blogpage]: http://www.railsbling.com/tags/rubocop-lts/
[codecov_coverage]: https://codecov.io/gh/rubocop-lts/rubocop-lts
[code_triage]: https://www.codetriage.com/rubocop-lts/rubocop-lts
[chat]: https://gitter.im/rubocop-lts/rubocop-lts?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge&utm_content=badge
[climate_coverage]: https://codeclimate.com/github/rubocop-lts/rubocop-lts/test_coverage
[climate_maintainability]: https://codeclimate.com/github/rubocop-lts/rubocop-lts/maintainability
[copyright-notice-explainer]: https://opensource.stackexchange.com/questions/5778/why-do-licenses-such-as-the-mit-license-specify-a-single-year
[conduct]: https://gitlab.com/rubocop-lts/rubocop-lts/-/blob/main/CODE_OF_CONDUCT.md
[contributing]: https://gitlab.com/rubocop-lts/rubocop-lts/-/blob/main/CONTRIBUTING.md
[devto]: https://dev.to/galtzo
[documentation]: https://rubydoc.info/github/rubocop-lts/rubocop-lts/main
[followme]: https://img.shields.io/twitter/follow/galtzo.svg?style=social&label=Follow
[gh_discussions]: https://github.com/rubocop-lts/rubocop-lts/discussions
[gh_sponsors]: https://github.com/sponsors/pboling
[issues]: https://github.com/rubocop-lts/rubocop-lts/issues
[liberapay_donate]: https://liberapay.com/pboling/donate
[license]: LICENSE.txt
[license-ref]: https://opensource.org/licenses/MIT
[license-img]: https://img.shields.io/badge/License-MIT-green.svg
[peterboling]: http://www.peterboling.com
[pvc]: http://guides.rubygems.org/patterns/#pessimistic-version-constraint
[railsbling]: http://www.railsbling.com
[rubygems]: https://rubygems.org/gems/rubocop-lts
[security]: https://github.com/rubocop-lts/rubocop-lts/blob/main/SECURITY.md
[semver]: http://semver.org/
[source]: https://github.com/rubocop-lts/rubocop-lts/
[tweetme]: http://twitter.com/galtzo

🍿 Fun facts

Q: What does the team eat for breakfast each morning?

> Wild chicken eggs.  No, really.  Here in Indonesia they are called "telur kampung".  Also, jasmine tea.

🧙 Now do the thing

`bundle add rubocop-lts`
