---
layout: page
title: Convention Over Configuration
permalink: /CONV_OVER_CONF/
---

<p align="center">
    <a href="https://rubocop.org#gh-light-mode-only"  target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/rubocop-light.svg?raw=true" alt="SVG RuboCop Logo, Copyright (c) 2014 Dimiter Petrov, CC BY-NC 4.0, see docs/images/logo/README.txt">
    </a>
    <a href="https://www.ruby-lang.org/" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/ruby-logo.svg?raw=true" alt="Yukihiro Matsumoto, Ruby Visual Identity Team, CC BY-SA 2.5, see docs/images/logo/README.txt">
    </a>
    <a href="https://semver.org/#gh-light-mode-only" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/semver-light.svg?raw=true" alt="SemVer.org Logo by @maxhaz, see docs/images/logo/README.txt">
    </a>
</p>

# 🦾 RuboCop LTS - Rules for Rubies: Rubocop + Standard + Betterlint + Shopify + Gradual

💡 See the intro [blog post](https://dev.to/pboling/rubocop-ruby-matrix-gems-nj)!

---

The **RuboCop LTS** family of gems is the distillation of almost 20 years
of my own Ruby expertise and source code diving,
built on the shoulders of the expertise of many others;
organizing that expertise into per-Ruby-version sets of configurations.

Although the situation has improved somewhat,
it remains [_unsafe_ to upgrade RuboCop, or Standard][Why-Build-This],
in a project that supports EOL Rubies.

I hope it helps others avoid some of the challenges I've had with library maintenance,
and supporting decade-old mission-critical applications.

Avoid bike-shedding, use `rubocop-lts` in every project, and
let it manage your linting complexity!

If the `rubocop-lts` stack of libraries has helped you, or your organization,
please support my efforts by making a donation, or becoming a sponsor.

[Why-Build-This]: https://rubocop-lts.gitlab.io/about/#why-build-this-

<div id="badges">

[![Liberapay Patrons][⛳liberapay-img]][⛳liberapay]
[![Sponsor Me on Github][🖇sponsor-img]][🖇sponsor]

<span class="badge-buymeacoffee">
<a href="https://ko-fi.com/O5O86SNP4" target='_blank' title="Donate to my FLOSS or refugee efforts at ko-fi.com"><img src="https://img.shields.io/badge/buy%20me%20coffee-donate-yellow.svg" alt="Buy me coffee donation button" /></a>
</span>
<span class="badge-patreon">
<a href="https://patreon.com/galtzo" title="Donate to my FLOSS or refugee efforts using Patreon"><img src="https://img.shields.io/badge/patreon-donate-yellow.svg" alt="Patreon donate button" /></a>
</span>

</div>

## 🗿 Conventions > Configuration

The major version you need depends on the supported version(s) of Ruby for your project.

### Supported Rubies

Odd major version releases have been deprecated,
and no further add major releases are planned.

All future releases will be even major releases.

### EOL Rubies

RuboCop needs to run on a recent,
non-EOL ruby in order to apply the rules for an older EOL version of ruby.

### Version Conventions

#### Even Major Release

Versions like:
```ruby
"0.x" # Runs on Ruby 2.7+ and analyzes Ruby 1.8
"2.x" # Runs on Ruby 2.7+ and analyzes Ruby 1.9
"4.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.0
"6.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.1
"8.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.2
"10.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.3
"12.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.4
"14.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.5
"16.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.6
"18.x" # Runs on Ruby 2.7+ and analyzes Ruby 2.7
"20.x" # Runs on Ruby 3.0+ and analyzes Ruby 3.0
"22.x" # Runs on Ruby 3.1+ and analyzes Ruby 3.1
"24.x" # Runs on Ruby 3.2+ and analyzes Ruby 3.2
```

`required_ruby_version` is locked to the forward range of Rubies on which the gem can be installed and run,
but the analysis only targets a single minor version of Ruby.
E.g. version 16.0 has a `required_ruby_version` of `'>= 2.7'` will install on any version
of ruby from `2.7` on, but it will analyze based on rules intended for Ruby 2.6.

#### Odd Major Release

These are deprecated.  Please upgrade to an even version release.

## Usage

Intended for applications and libraries that lint against a range of Ruby versions.

Even versions will help projects upgrade to newer Rubies while keeping the same underlying version of RuboCop version, so change can be introduced one step at a time.

| Minimum Runtime Ruby | Your Gemfile                   | Your Gemfile.lock | Your .rubocop.yml                                                                             |
|----------------------|--------------------------------|-------------------|-----------------------------------------------------------------------------------------------|
| `1.8.x`              | `gem "rubocop-lts", "~> 0.0"`  | `rubocop-ruby1_8` | `inherit_gem:\n  rubocop-lts: rubocop-lts.yml` (note: or one of the other options in config/) |
| `1.9.x`              | `gem "rubocop-lts", "~> 2.0"`  | `rubocop-ruby1_9` | 👆️ (no change)                                                                               |
| `2.0.x`              | `gem "rubocop-lts", "~> 4.0"`  | `rubocop-ruby2_0` | 👆️ (no change)                                                                               |
| `2.1.x`              | `gem "rubocop-lts", "~> 6.0"`  | `rubocop-ruby2_1` | 👆️ (no change)                                                                               |
| `2.2.x`              | `gem "rubocop-lts", "~> 8.0"`  | `rubocop-ruby2_2` | 👆️ (no change)                                                                               |
| `2.3.x`              | `gem "rubocop-lts", "~> 10.0"` | `rubocop-ruby2_3` | 👆️ (no change)                                                                               |
| `2.4.x`              | `gem "rubocop-lts", "~> 12.0"` | `rubocop-ruby2_4` | 👆️ (no change)                                                                               |
| `2.5.x`              | `gem "rubocop-lts", "~> 14.0"` | `rubocop-ruby2_5` | 👆️ (no change)                                                                               |
| `2.6.x`              | `gem "rubocop-lts", "~> 16.0"` | `rubocop-ruby2_6` | 👆️ (no change)                                                                               |
| `2.7.x`              | `gem "rubocop-lts", "~> 18.0"` | `rubocop-ruby2_7` | 👆️ (no change)                                                                               |
| `3.0.x`              | `gem "rubocop-lts", "~> 20.0"` | `rubocop-ruby3_0` | 👆️ (no change)                                                                               |
| `3.1.x`              | `gem "rubocop-lts", "~> 22.0"` | `rubocop-ruby3_1` | 👆️ (no change)                                                                               |
| `3.2.x`              | `gem "rubocop-lts", "~> 24.0"` | `rubocop-ruby3_2` | 👆️ (no change)                                                                               |

### Options

Default loads the configs for the relevant version of Ruby, and RSpec.
```yaml
inherit_gem:
  rubocop-lts: rubocop-lts.yml
```

In other words, the default is equivalent to the following:
```yaml
inherit_gem:
  rubocop-lts: config/ruby_rspec.yml
```

If you are using Ruby, but not RSpec, there's a config for that.
```yaml
inherit_gem:
  rubocop-lts: config/ruby.yml
```

If you are building a RubyGem try this:
```yaml
inherit_gem:
  rubocop-lts: config/rubygem.yml
```

If your RubyGem is tested with RSpec, simply do:
```yaml
inherit_gem:
  rubocop-lts: config/rubygem_rspec.yml
```

If you are building a Rails app, then copy paste:
```yaml
inherit_gem:
  rubocop-lts: config/rails.yml
```

If your Rails app is tested with RSpec, then do one better:
```yaml
inherit_gem:
  rubocop-lts: config/rails_rspec.yml
```

If you only want the RSpec configs:
```yaml
inherit_gem:
  rubocop-lts: config/rspec.yml
```

If you have ideas for other "collections of rules", I'm open to suggestions.
I've thought about a minitest collection, but I haven't used it much.

### Tracking Branches

Each major version will have a tracking branch named accordingly, [for development](https://github.com/rubocop-lts/rubocop-lts#git-branch-names).
