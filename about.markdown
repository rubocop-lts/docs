---
layout: page
title: About
permalink: /about/
---

<p align="center">
    <a href="https://rubocop.org#gh-light-mode-only"  target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/rubocop-light.svg?raw=true" alt="SVG RuboCop Logo, Copyright (c) 2014 Dimiter Petrov, CC BY-NC 4.0, see docs/images/logo/README.txt">
    </a>
    <a href="https://www.ruby-lang.org/" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/ruby-logo.svg?raw=true" alt="Yukihiro Matsumoto, Ruby Visual Identity Team, CC BY-SA 2.5, see docs/images/logo/README.txt">
    </a>
    <a href="https://semver.org/#gh-light-mode-only" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/semver-light.svg?raw=true" alt="SemVer.org Logo by @maxhaz, see docs/images/logo/README.txt">
    </a>
</p>

# RuboCop LTS - Rules for Rubies: Rubocop + Standard + Betterlint + Shopify + Gradual

💡 See the intro [blog post](https://dev.to/pboling/rubocop-ruby-matrix-gems-nj)!

---

The **RuboCop LTS** family of gems is the distillation of almost 20 years
of my own Ruby expertise and source code diving,
built on the shoulders of the expertise of many others;
organizing that expertise into per-Ruby-version sets of configurations.

Although the situation has improved somewhat,
it remains [_unsafe_ to upgrade RuboCop, or Standard][Why-Build-This],
in a project that supports EOL Rubies.

I hope it helps others avoid some of the challenges I've had with library maintenance,
and supporting decade-old mission-critical applications.

Avoid bike-shedding, use `rubocop-lts` in every project, and
let it manage your linting complexity!

If the `rubocop-lts` stack of libraries has helped you, or your organization,
please support my efforts by making a donation, or becoming a sponsor.

[Why-Build-This]: https://rubocop-lts.gitlab.io/about/#why-build-this-

<div id="badges">

[![Liberapay Patrons][⛳liberapay-img]][⛳liberapay]
[![Sponsor Me on Github][🖇sponsor-img]][🖇sponsor]

<span class="badge-buymeacoffee">
<a href="https://ko-fi.com/O5O86SNP4" target='_blank' title="Donate to my FLOSS or refugee efforts at ko-fi.com"><img src="https://img.shields.io/badge/buy%20me%20coffee-donate-yellow.svg" alt="Buy me coffee donation button" /></a>
</span>
<span class="badge-patreon">
<a href="https://patreon.com/galtzo" title="Donate to my FLOSS or refugee efforts using Patreon"><img src="https://img.shields.io/badge/patreon-donate-yellow.svg" alt="Patreon donate button" /></a>
</span>

</div>

[⛳liberapay-img]: https://img.shields.io/liberapay/patrons/pboling.svg?logo=liberapay
[⛳liberapay]: https://liberapay.com/pboling/donate
[🖇sponsor-img]: https://img.shields.io/badge/Sponsor_Me!-pboling.svg?style=social&logo=github
[🖇sponsor]: https://github.com/sponsors/pboling

---

## Why Build This?

### ⁉️ What about Standard? It provides ruby-version-specific RuboCop rules, doesn't it?

Actually, not in the sense you might have assumed if you are asking that question.

I assumed as much when I first became aware of the discrete configs
for specific versions of Ruby via Evil Martians' write ups
([here (rubocop + standard)](https://evilmartians.com/chronicles/rubocoping-with-legacy-bring-your-ruby-code-up-to-standard),
and [here (adds rubocop-gradual)](https://dev.to/evilmartians/rubocoping-with-legacy-gme)).
However, the discrete configs, which go all the way back to Ruby 1.8,
are *not* intended to keep a code base "compatible" with the specified version of Ruby.
According to Justin Searls, the project's author and lead maintainer,
Standard's ruby-specific [configs merely **disable** rules](https://mastodon.social/@searls/110454153031690829)
where keeping Standard's more modern style would "break" the older Ruby.

In other words, they _do not_ attempt to actively ensure compliance with any older Ruby syntax,
even when that is possible, and advisable, for a project targeting long-term support of older Ruby versions.

For example standard does not:

- set `Layout/DotPosition` to `EnforcedStyle: trailing` for Ruby 1.8 compatibility, nor does it
- set `Style/SymbolArray` and `Style/WordArray` to `EnforcedStyle: brackets` for Ruby 1.8 and 1.9 compatibility.

Instead, these style enforcers are completely disabled.

This is why you need `rubocop-lts`.  It is built on top of `standard-rubocop-lts`,
which shims standard's rules to enforce maximum compatibility for targeted versions of Old Ruby.

You get the standard rules as much as possible, but without breaking your old Ruby compatibility.

Also, I've added a bunch of other linter rules on top of Standard, e.g. Shopify's rules.

This is still an anti-bike-shedding tool, but my goal is not to have fewer rules.
My goal is to be able to integrate this tool with a code base, on any version of Ruby, in as few lines as possible.

### 🙋‍♀️ How often has RuboCop broken your build?

This is both good (literally its job) _and_ bad (when it's for the wrong reasons).

It's supposed to break the build when it finds violations.
It should not break the build due to incompatibility with your environment.  RuboCop
doesn't _exactly_ [follow SemVer](https://dev.to/pboling/rubocop-ruby-matrix-gems-nj),
and occasionally it will unexpectedly break things.

Here's a [recent example](https://github.com/sunny/actor/pull/65#pullrequestreview-999365028)
of a scenario that could have been avoided, and from now on will be!

