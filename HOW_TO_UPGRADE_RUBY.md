---
layout: page
title: How to Upgrade Ruby
permalink: /HOW_TO_UPGRADE_RUBY/
---

<p align="center">
    <a href="https://rubocop.org#gh-light-mode-only"  target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/rubocop-light.svg?raw=true" alt="SVG RuboCop Logo, Copyright (c) 2014 Dimiter Petrov, CC BY-NC 4.0, see docs/images/logo/README.txt">
    </a>
    <a href="https://www.ruby-lang.org/" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/ruby-logo.svg?raw=true" alt="Yukihiro Matsumoto, Ruby Visual Identity Team, CC BY-SA 2.5, see docs/images/logo/README.txt">
    </a>
    <a href="https://semver.org/#gh-light-mode-only" target="_blank" rel="noopener">
      <img height="120px" src="https://github.com/rubocop-lts/rubocop-lts/raw/main/docs/images/logo/semver-light.svg?raw=true" alt="SemVer.org Logo by @maxhaz, see docs/images/logo/README.txt">
    </a>
</p>

# 🦾 RuboCop LTS - Rules for Rubies: Rubocop + Standard + Betterlint + Shopify + Gradual

💡 See the intro [blog post](https://dev.to/pboling/rubocop-ruby-matrix-gems-nj)!

---

The **RuboCop LTS** family of gems is the distillation of almost 20 years
of my own Ruby expertise and source code diving,
built on the shoulders of the expertise of many others;
organizing that expertise into per-Ruby-version sets of configurations.

Although the situation has improved somewhat,
it remains [_unsafe_ to upgrade RuboCop, or Standard][Why-Build-This],
in a project that supports EOL Rubies.

I hope it helps others avoid some of the challenges I've had with library maintenance,
and supporting decade-old mission-critical applications.

Avoid bike-shedding, use `rubocop-lts` in every project, and
let it manage your linting complexity!

If the `rubocop-lts` stack of libraries has helped you, or your organization,
please support my efforts by making a donation, or becoming a sponsor.

[Why-Build-This]: https://rubocop-lts.gitlab.io/about/#why-build-this-

<div id="badges">

[![Liberapay Patrons][⛳liberapay-img]][⛳liberapay]
[![Sponsor Me on Github][🖇sponsor-img]][🖇sponsor]

<span class="badge-buymeacoffee">
<a href="https://ko-fi.com/O5O86SNP4" target='_blank' title="Donate to my FLOSS or refugee efforts at ko-fi.com"><img src="https://img.shields.io/badge/buy%20me%20coffee-donate-yellow.svg" alt="Buy me coffee donation button" /></a>
</span>
<span class="badge-patreon">
<a href="https://patreon.com/galtzo" title="Donate to my FLOSS or refugee efforts using Patreon"><img src="https://img.shields.io/badge/patreon-donate-yellow.svg" alt="Patreon donate button" /></a>
</span>

</div>

[⛳liberapay-img]: https://img.shields.io/liberapay/patrons/pboling.svg?logo=liberapay
[⛳liberapay]: https://liberapay.com/pboling/donate
[🖇sponsor-img]: https://img.shields.io/badge/Sponsor_Me!-pboling.svg?style=social&logo=github
[🖇sponsor]: https://github.com/sponsors/pboling

---

## How To Upgrade Ruby (or, How To Untie Gorgon's Knot 🪢)

📝 The ruby specific versions, e.g. `rubocop-ruby1_8`, or `rubocop-ruby3_2`,
can be used if you won't be upgrading ruby.

| Your Ruby is  | Your Gemfile has               | Ruby version for linting | Your Gemfile.lock gets       | Your .rubocop.yml needs:                       |
|---------------|--------------------------------|--------------------------|------------------------------|------------------------------------------------|
| `1.8.x`       | `gem "rubocop-lts", "~> 0.1"`  | `'>= 2.7'`               | [`rubocop-ruby1_8`][⛳️18-gh] | `inherit_gem:\n  rubocop-lts: rubocop-lts.yml` |
| ⬆️ to `1.9.x` | `gem "rubocop-lts", "~> 2.1"`  | `'>= 2.7'`               | [`rubocop-ruby1_9`][⛳️19-gh] | no change                                      |
| ⬆️ to `2.0.x` | `gem "rubocop-lts", "~> 4.1"`  | `'>= 2.7'`               | [`rubocop-ruby2_0`][⛳️20-gh] | no change                                      |
| ⬆️ to `2.1.x` | `gem "rubocop-lts", "~> 6.1"`  | `'>= 2.7'`               | [`rubocop-ruby2_1`][⛳️21-gh] | no change                                      |
| ⬆️ to `2.2.x` | `gem "rubocop-lts", "~> 8.1"`  | `'>= 2.7'`               | [`rubocop-ruby2_2`][⛳️22-gh] | no change                                      |
| ⬆️ to `2.3.x` | `gem "rubocop-lts", "~> 10.1"` | `'>= 2.7'`               | [`rubocop-ruby2_3`][⛳️23-gh] | no change                                      |
| ⬆️ to `2.4.x` | `gem "rubocop-lts", "~> 12.1"` | `'>= 2.7'`               | [`rubocop-ruby2_4`][⛳️24-gh] | no change                                      |
| ⬆️ to `2.5.x` | `gem "rubocop-lts", "~> 14.1"` | `'>= 2.7'`               | [`rubocop-ruby2_5`][⛳️25-gh] | no change                                      |
| ⬆️ to `2.6.x` | `gem "rubocop-lts", "~> 16.1"` | `'>= 2.7'`               | [`rubocop-ruby2_6`][⛳️26-gh] | no change                                      |
| ⬆️ to `2.7.x` | `gem "rubocop-lts", "~> 18.2"` | `'>= 2.7'`               | [`rubocop-ruby2_7`][⛳️27-gh] | no change                                      |
| ⬆️ to `3.0.x` | `gem "rubocop-lts", "~> 20.2"` | `'>= 3.0'`               | [`rubocop-ruby2_0`][⛳️30-gh] | no change                                      |
| ⬆️ to `3.1.x` | `gem "rubocop-lts", "~> 22.1"` | `'>= 3.1'`               | [`rubocop-ruby3_1`][⛳️31-gh] | no change                                      |
| ⬆️ to `3.2.x` | `gem "rubocop-lts", "~> 24.0"` | `'>= 3.2'`               | [`rubocop-ruby3_2`][⛳️32-gh] | no change                                      |

## 📼 Supporting EOL Rubies

You can only run linting when running Ruby >= 2.7.  Your project can support older rubies,
since Ruby code can be written that supports all versions from 1.8 to 3.X (as many popular gems do, including RSpec).
There is no need to run the linter on every Ruby version. Just pick a version, and run the linter once,
using a ruleset that supports older Ruby syntax, which this gem family provides.

[⛳️lts-gh]: https://github.com/rubocop-lts/rubocop-lts
[⛳️18-gh]: https://github.com/rubocop-lts/rubocop-ruby1_8
[⛳️19-gh]: https://github.com/rubocop-lts/rubocop-ruby1_9
[⛳️20-gh]: https://github.com/rubocop-lts/rubocop-ruby2_0
[⛳️21-gh]: https://github.com/rubocop-lts/rubocop-ruby2_1
[⛳️22-gh]: https://github.com/rubocop-lts/rubocop-ruby2_2
[⛳️23-gh]: https://github.com/rubocop-lts/rubocop-ruby2_3
[⛳️24-gh]: https://github.com/rubocop-lts/rubocop-ruby2_4
[⛳️25-gh]: https://github.com/rubocop-lts/rubocop-ruby2_5
[⛳️26-gh]: https://github.com/rubocop-lts/rubocop-ruby2_6
[⛳️27-gh]: https://github.com/rubocop-lts/rubocop-ruby2_7
[⛳️30-gh]: https://github.com/rubocop-lts/rubocop-ruby3_0
[⛳️31-gh]: https://github.com/rubocop-lts/rubocop-ruby3_1
[⛳️32-gh]: https://github.com/rubocop-lts/rubocop-ruby3_2
